void buttonpush()
{
  // read the pushbutton input pin:
  buttonState = digitalRead(encoderSwitchPin);

  // compare the buttonState to its previous state
  if (buttonState != lastButtonState) {


    if (timerbutton > 50)
    {
      // if the state has changed, increment the counter
      if (buttonState == HIGH) {
        // if the current state is HIGH then the button
        // went from off to on:
        buttonstate2 = 0;

        //button is pressed do something !!
      }

      else if (buttonState == LOW)
      {
        // if the current state is LOW then the button
        // went from on to off:
        buttonstate2 = 1;
      }
      // Delay a little bit to avoid bouncing
      //   delay(50);
    }
  }

  // save the current state as the last state,
  //for next time through the loop
  lastButtonState = buttonState;
}