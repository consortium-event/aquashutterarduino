void menu()
{

  switch (menupos)
  {
    case 0: //DMX Address alter------------------------------------------------------------------------------------------------------------------------------

      blinkingcursor(1);

      if (firsttime == 1)
      {
        menusession = 0;
        maxavaliableencoderpositions = 503;
        minavaliableencoderpositions = 1;

        myEnc.write(dmxaddress * 4);
        firsttime = 0;
        timer = 0;
      }

      dmxaddress_zw = encoder();

      if (dmxaddress_zw != dmxaddress)
      {
        menusession = 0;
        dmxaddress = dmxaddress_zw;
        storedmxaddress();
        showdmxaddress();
        blinkingcursor(1);
      }

      if ((buttonstate2 == 1 ) && (timer > 500))  //end alter
      {
        enddmxalter();
      }


      break;

    case 1: //DMX Address display-----------------------------------------------------------------------------------------------------------------

      if (firsttime == 1)
      {
        clearlcd();
        menusession = 0;
        blinkingcursor(0);
        myEnc.write(4);
        setcursor(2);
        Serial4.print("DMX Address:");
        delay(10);
        timer = 0;
        showdmxaddress();
        firsttime = 0;
      }

      if ((buttonstate2 == 1) && (timer > 500))
      {

        menusession = 0;
        dmxalter = 1;
        firsttime = 1;
        timer = 0;
        menupos = 0; //switch to dmx address alter
        break;
      }

      if ((menupos != encoder()) && (dmxalter == 0) && (menuactive == 1))
      {
        menusession = 0;
        firsttime = 1;
        menupos = encoder();
        break;
      }

      break;

    case 2: //fulltime display----------------------------------------------------------------------------------------------

      if (firsttime == 1)
      {
        clearlcd();
        menusession = 0;
        blinkingcursor(0);
        myEnc.write(8);
        setcursor(2);
        Serial4.print("Full-Time Limit:");
        delay(10);
        timer = 0;
        showfulltime();
        firsttime = 0;
      }

      if ((buttonstate2 == 1) && (timer > 500))
      {

        menusession = 0;
        fulltimealter = 1;
        firsttime = 1;
        timer = 0;
        menupos = 5; //switch to on-time alter
        break;
      }

      if ((menupos != encoder()) && (dmxalter == 0) && (menuactive == 1))
      {
        menusession = 0;
        firsttime = 1;
        menupos = encoder();
        break;
      }
      break;

    case 3: //holdvoltage display----------------------------------------------------------------------------------------------------------------

      if (firsttime == 1)
      {
        clearlcd();
        menusession = 0;
        blinkingcursor(0);
        myEnc.write(12);
        setcursor(2);
        Serial4.print("turnback ratio:");
        delay(10);
        timer = 0;
        showholdvoltage();
        firsttime = 0;
      }

      if ((buttonstate2 == 1) && (timer > 500))
      {

        menusession = 0;
        holdvoltagealter = 1;
        firsttime = 1;
        timer = 0;
        menupos = 6; //switch to off-time alter
        break;
      }

      if ((menupos != encoder()) && (dmxalter == 0) && (menuactive == 1))
      {
        menusession = 0;
        firsttime = 1;
        menupos = encoder();
        break;
      }
      break;

    case 4: //turnoff display----------------------------------------------------------------

      if (firsttime == 1)
      {
        clearlcd();
        menusession = 0;
        blinkingcursor(0);
        myEnc.write(16);
        setcursor(2);
        Serial4.print("turnoff time:");
        delay(10);
        timer = 0;
        showturnoff();
        firsttime = 0;
      }

      if ((buttonstate2 == 1) && (timer > 500))
      {
        menusession = 0;
        turnoffalter = 1;
        firsttime = 1;
        timer = 0;
        menupos = 7; //switch to turnoff alter
        break;
      }

      if ((menupos != encoder()) && (dmxalter == 0) && (menuactive == 1))
      {
        menusession = 0;
        firsttime = 1;
        menupos = encoder();
        break;
      }
      break;

    case 5: //fulltime alter-----------------------------------------------------------------------------------------------------------------

      blinkingcursor(1);

      if (firsttime == 1)
      {
        menusession = 0;
        setcursor(4);
        movecursorright(19);
        menusession = 0;
        maxavaliableencoderpositions = 999;
        minavaliableencoderpositions = 1;
        myEnc.write(fulltime * 4);
        firsttime = 0;
        timer = 0;
      }

      fulltime_zw = encoder();

      if (fulltime_zw != fulltime)
      {
        menusession = 0;
        fulltime = fulltime_zw;
        storefulltime();
        showfulltime();
        blinkingcursor(1);
      }

      if ((buttonstate2 == 1 ) && (timer > 500))  //end alter
      {
        menusession = 0;
        endfulltimealter();
      }

      break;

    case 6: //holdvoltage alter----------------------------------------------------------------------------------------------------------------

      blinkingcursor(1);

      if (firsttime == 1)
      {
        menusession = 0;
        setcursor(4);
        movecursorright(19);
        menusession = 0;
        maxavaliableencoderpositions = 100;
        minavaliableencoderpositions = 1;
        myEnc.write(holdvoltage * 4);
        firsttime = 0;
        timer = 0;
      }

      holdvoltage_zw = encoder();

      if (holdvoltage_zw != holdvoltage)
      {
        menusession = 0;
        holdvoltage = holdvoltage_zw;
        storeholdvoltage();
        showholdvoltage();
        blinkingcursor(1);
      }

      if ((buttonstate2 == 1 ) && (timer > 500))  //end alter
      {
        menusession = 0;
        endholdvoltagealter();
      }
      break;

    case 7: //turnoff alter---------------------------------------------------------------------------------------------------------------------------------

      blinkingcursor(1);

      if (firsttime == 1)
      {
        menusession = 0;
        setcursor(4);
        movecursorright(19);
        menusession = 0;
        maxavaliableencoderpositions = 100;
        minavaliableencoderpositions = 1;
        myEnc.write(turnoff * 4);
        firsttime = 0;
        timer = 0;
      }

      turnoff_zw = encoder();

      if (turnoff_zw != turnoff)
      {
        menusession = 0;
        turnoff = turnoff_zw;
        storeturnoff();
        showturnoff();
        blinkingcursor(1);
      }

      if ((buttonstate2 == 1 ) && (timer > 500))  //end alter
      {
        menusession = 0;
        endturnoffalter();
      }
      break;


    case 10:

      break;
  }
}

