void fadeinlcd()
{
  for (int j = 1; j <= 8; j++)
  {
    brightnesslcd(j);
    delay(20);
  }
}

void fadeoutlcd()
{
  for (int j = 8; j >= 1; j--)
  {
    brightnesslcd(j);
    delay(20);
  }
}

void deletechar(int num)
{
  for (int j = 1; j <= num; j++)
  {
    x = 0xFE;
    Serial4.print(x);

    x = 0x4E;
    Serial4.print(x);

    //delay(5);
  }
}

void clearlcd()
{
  x = 0xFE;
  Serial4.print(x);

  x = 0x51;
  Serial4.print(x);

  //delay(5);
}

void setcursor(int n)
{
  x = 0xFE;
  Serial4.print(x);

  x = 0x45;
  Serial4.print(x);

  if (n > 4)
  {
    n = n % 4;
  }

  if (n == 1)
  {
    x = 0x00;
    Serial4.print(x);
    //delay(5);
  }
  else if (n == 2)
  {
    x = 0x40;
    Serial4.print(x);
    //delay(5);
  }
  else if (n == 3)
  {
    x = 0x14;
    Serial4.print(x);
    //delay(5);
  }
  else if (n == 4)
  {
    x = 0x54;
    Serial4.print(x);
    //delay(5);
  }
}

void hidelcdtext(bool wert)
{
  if (wert == 1)
  {
    x = 0xFE;
    Serial4.print(x);

    x = 0x42;
    Serial4.print(x);

    //delay(5);
  }

  else if (wert == 0)
  {
    x = 0xFE;
    Serial4.print(x);

    x = 0x41;
    Serial4.print(x);

    //delay(5);
  }
}

void brightnesslcd(int br)
{
  x = 0xFE;
  Serial4.print(x);

  x = 0x53;
  Serial4.print(x);

  x = br;
  Serial4.print(x);

  lcdbrightness = br;
  //delay(5);
}

void movecursorright(int mo)
{
  for (int j = 1; j <= mo; j++)
  {
    x = 0xFE;
    Serial4.print(x);

    x = 0x4A;
    Serial4.print(x);

    //delay(5);
  }
}

void blinkingcursor(bool isit)
{
  if (isit == 1)
  {
    x = 0xFE;
    Serial4.print(x);

    x = 0x4B;
    Serial4.print(x);

    //delay(5);
  }

  else
  {
    x = 0xFE;
    Serial4.print(x);

    x = 0x4C;
    Serial4.print(x);

    //delay(5);
  }
}

