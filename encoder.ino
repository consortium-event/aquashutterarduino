int encoder()
{
  int newPosition = myEnc.read(); //reads the state of the encoder

  delay(15);

  if (newPosition != oldPosition) {

    oldPosition = newPosition;

    encodervaluenow = newPosition / 4;

    if (encodervaluenow != encodervaluethen)
    {
      if ((encodervaluenow <= maxavaliableencoderpositions) && (encodervaluenow >= minavaliableencoderpositions))
      {
        //read out encoder position here
        if (timerencoder > 90)
        {
          encodervaluethen = newPosition / 4;
          timerencoder = 0;
        }
      }


      if (encodervaluenow > maxavaliableencoderpositions)
      {
        myEnc.write(minavaliableencoderpositions * 4);
        delay(15);
      }

      if (encodervaluenow < minavaliableencoderpositions)
      {
        myEnc.write(maxavaliableencoderpositions * 4);
        delay(15);
      }
    }
  }

  return encodervaluethen;
}

