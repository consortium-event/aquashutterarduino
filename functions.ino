void dmxaddress_read_from_eeprom()
{
  if ((dmxzw < 255) && (dmxzw3 == 0))
  {
    dmxaddress = dmxzw;
  }

  else if (dmxzw == 255)
  {
    dmxaddress = dmxzw + dmxzw2;
  }

  else if (dmxzw3 == 1)
  {
    dmxaddress = 512;
  }
}

void fulltime_read_from_eeprom()
{
  if ((fulltime1 <= 255) && (fulltime2 == 0))
  {
    fulltime = fulltime1;
  }

  else if ((fulltime2 <= 255) && (fulltime3 == 0))
  {
    fulltime = 255 + fulltime2;
  }

  else if ((fulltime3 <= 255) && (fulltime4 == 0))
  {
    fulltime = 510 + fulltime3;
  }

  else if (fulltime4 <= 255)
  {
    fulltime = 765 + fulltime4;
  }
}

void holdvoltage_read_from_eeprom()
{
  if ((holdvoltage1 <= 255) && (holdvoltage2 == 0))
  {
    holdvoltage = holdvoltage1;
  }

  else if ((holdvoltage2 <= 255) && (holdvoltage3 == 0))
  {
    holdvoltage = 255 + holdvoltage2;
  }

  else if ((holdvoltage3 <= 255) && (holdvoltage4 == 0))
  {
    holdvoltage = 510 + holdvoltage3;
  }

  else if (holdvoltage4 <= 255)
  {
    holdvoltage = 765 + holdvoltage4;
  }
}

void turnoff_read_from_eeprom()
{
  if ((turnoff1 <= 255) && (turnoff2 == 0))
  {
    turnoff = turnoff1;
  }

  else if ((turnoff2 <= 255) && (turnoff3 == 0))
  {
    turnoff = 255 + turnoff2;
  }

  else if ((turnoff3 <= 255) && (turnoff4 == 0))
  {
    turnoff = 510 + turnoff3;
  }

  else if (turnoff4 <= 255)
  {
    turnoff = 765 + turnoff4;
  }
}

void showdmxaddress()
{
  blinkingcursor(0);
  setcursor(4);
  movecursorright(17);

  deletechar(3);

  if (dmxaddress < 10)
  {
    Serial4.print("00");
  }
  else if (dmxaddress < 100)
  {
    Serial4.print("0");
  }

  Serial.println(dmxaddress);

  Serial4.print(dmxaddress);
  // delay(10);
}

void showfulltime()
{
  blinkingcursor(0);

  setcursor(4);
  movecursorright(16);

  deletechar(3);

  if (fulltime < 100)
  {
    Serial4.print("0");
  }

  if (fulltime < 10)
  {
    Serial4.print("0");
  }

  Serial.print(fulltime);
  Serial.println(" ms");

  Serial4.print(fulltime);
  Serial4.print(" ms");
  // delay(10);
}

void showholdvoltage()
{
  blinkingcursor(0);
  setcursor(4);
  movecursorright(17);

  deletechar(3);


  if (holdvoltage < 100)
  {
    Serial4.print("0");
  }

  if (holdvoltage < 10)
  {
    Serial4.print("0");
  }

  Serial.print(holdvoltage);
  Serial.println(" %");

  Serial4.print(holdvoltage);
  Serial4.print(" %");
  // delay(10);
}

void showturnoff()
{
  blinkingcursor(0);
  setcursor(4);
  movecursorright(16);

  deletechar(3);


  if (turnoff < 100)
  {
    Serial4.print("0");
  }

  if (turnoff < 10)
  {
    Serial4.print("0");
  }

  Serial.print(turnoff);
  Serial.println(" ms");

  Serial4.print(turnoff);
  Serial4.print(" ms");
  // delay(10);
}

void enddmxalter()
{
  menusession = 0;
  dmxalter = 0;

  for (int i = 0; i < 10; i++)
  {
    DMXvalues[i] = 0;
  }

  maxavaliableencoderpositions = 4;
  minavaliableencoderpositions = 1;

  myEnc.write(4);

  blinkingcursor(0);
  firsttime = 1;
  timer = 0;
  menupos = 1;
}

void storedmxaddress()
{
  if (dmxaddress <= 255)
  {
    EEPROM.update(0, dmxaddress);
    EEPROM.update(1, 0);
    EEPROM.update(2, 0);
  }

  else if (dmxaddress > 255)
  {
    EEPROM.update(0, 255);
    EEPROM.update(1, (dmxaddress - 255));
    EEPROM.update(2, 0);
  }

  if (dmxaddress == 512)
  {
    EEPROM.update(0, 0);
    EEPROM.update(1, 0);
    EEPROM.update(2, 1);
  }
}

void storefulltime()
{
  if (fulltime <= 255)
  {
    EEPROM.update(4, fulltime);
    EEPROM.update(5, 0);
    EEPROM.update(6, 0);
    EEPROM.update(7, 0);
  }

  else if ((fulltime > 255) && (fulltime <= 512))
  {
    EEPROM.update(4, 255);
    EEPROM.update(5, fulltime - 255);
    EEPROM.update(6, 0);
    EEPROM.update(7, 0);
  }

  else if ((fulltime > 510) && (fulltime <= 765))
  {
    EEPROM.update(4, 255);
    EEPROM.update(5, 255);
    EEPROM.update(6, fulltime - 510);
    EEPROM.update(7, 0);
  }

  else if (fulltime > 765)
  {
    EEPROM.update(4, 255);
    EEPROM.update(5, 255);
    EEPROM.update(6, 255);
    EEPROM.update(7, fulltime - 765);
  }
}

void storeholdvoltage()
{
  if (holdvoltage <= 255)
  {
    EEPROM.update(9, holdvoltage);
    EEPROM.update(10, 0);
    EEPROM.update(11, 0);
    EEPROM.update(12, 0);
  }

  else if ((holdvoltage > 255) && (holdvoltage <= 512))
  {
    EEPROM.update(9, 255);
    EEPROM.update(10, holdvoltage - 255);
    EEPROM.update(11, 0);
    EEPROM.update(12, 0);
  }

  else if ((holdvoltage > 510) && (holdvoltage <= 765))
  {
    EEPROM.update(9, 255);
    EEPROM.update(10, 255);
    EEPROM.update(11, holdvoltage - 510);
    EEPROM.update(12, 0);
  }

  else if (holdvoltage > 765)
  {
    EEPROM.update(9, 255);
    EEPROM.update(10, 255);
    EEPROM.update(11, 255);
    EEPROM.update(12, holdvoltage - 765);
  }

  valuehb = map(holdvoltage, 1, 100, 1, 256);
}

void storeturnoff()
{
  if (turnoff <= 255)
  {
    EEPROM.update(14, turnoff);
    EEPROM.update(15, 0);
    EEPROM.update(16, 0);
    EEPROM.update(17, 0);
  }

  else if ((turnoff > 255) && (turnoff <= 512))
  {
    EEPROM.update(14, 255);
    EEPROM.update(15, turnoff - 255);
    EEPROM.update(16, 0);
    EEPROM.update(17, 0);
  }

  else if ((turnoff > 510) && (turnoff <= 765))
  {
    EEPROM.update(14, 255);
    EEPROM.update(15, 255);
    EEPROM.update(16, turnoff - 510);
    EEPROM.update(17, 0);
  }

  else if (turnoff > 765)
  {
    EEPROM.update(14, 255);
    EEPROM.update(15, 255);
    EEPROM.update(16, 255);
    EEPROM.update(17, turnoff - 765);
  }
}

void endfulltimealter()
{
  menusession = 0;
  fulltimealter = 0;

  maxavaliableencoderpositions = 4;
  minavaliableencoderpositions = 1;

  myEnc.write(8);

  blinkingcursor(0);
  firsttime = 1;
  timer = 0;
  menupos = 2;
}

void endholdvoltagealter()
{
  menusession = 0;
  holdvoltagealter = 0;

  maxavaliableencoderpositions = 4;
  minavaliableencoderpositions = 1;

  myEnc.write(12);

  valuehb = map(holdvoltage, 1, 100, 1, 256);

  blinkingcursor(0);
  firsttime = 1;
  timer = 0;
  menupos = 3;
}

void endturnoffalter()
{
  menusession = 0;
  turnoffalter = 0;

  maxavaliableencoderpositions = 4;
  minavaliableencoderpositions = 1;

  myEnc.write(16);

  blinkingcursor(0);
  firsttime = 1;
  timer = 0;
  menupos = 4;
}

void changeHbridgestatus(int des, int state)
{
  ad1 = hbridgedes[des][0]; //get Pinnumber form array
  ad2 = hbridgedes[des][1]; //get Pinnumber from array

  if (state == 1) //turn on Hbridge
  {
    digitalWrite(ad1, HIGH);
    digitalWrite(ad2, LOW);

    Serial.print("Turned on H-Bridge");
    Serial.println(des + 1);
  }

  if (state == 0) //turn off Hrbidge
  {
    analogWrite(ad1, 0);
    analogWrite(ad2, 0);
  }

  if (state == 2) //turn back Hbridge
  {
    //Serial.println(valuehb);

    analogWrite(ad1, valuehb);
    analogWrite(ad2, 0);

    Serial.print("Turned back H-Bridge");
    Serial.println(des + 1);
  }

  if (state == 3)
  {
    digitalWrite(ad1, LOW);
    //digitalWrite(ad2, HIGH);
    digitalWrite(ad2, LOW);
  }
}

void updateMagnets()
{
  for (int i = 0; i < 10; i++)
  {
    //----------------------------------------------------------------------------------------------------
    if (magtimer[i][1] == 1) //check if ON-Bit is set
    {
      if ((magtimer[i][0] + fulltime) >= hbridgetimer) //check if fulltime has NOT passed
      {
        changeHbridgestatus(i, 1);
        help[i] = 1;
      }

      else //if it has passed ----> turn back Hbridge
      {
        if (help[i] == 1)
        {
          changeHbridgestatus(i, 2);              //tell Hbridge to turn back
          help[i] = 2;
        }
      }
    }

    //----------------------------------------------------------------------------------------------------

    else //if magtimer[i][1] == 0 ---> off command recieved
    {
      if (magtimer[i][1] == 0); //check if Hbridge received off command and isn´t turned off already
      {
        if ((magtimer[i][2] - magtimer[i][0]) > 1000)
        {
          floathelp[i][0] = float(magtimer[i][2]) + turnoff;
        }

        else
        {
          floathelp[i][0] = float(magtimer[i][2]) + 25;
        }

        if (floathelp[i][0] >= float(hbridgetimer)) //check if fulltime has NOT passed
        {
          changeHbridgestatus(i, 3);
          help[i] = 3;
        }

        else //if it has passed ----> turn back Hbridge
        {
          if (help[i] == 3)
          {
            changeHbridgestatus(i, 0);              //tell Hbridge to turn back
            help[i] = 0;
          }
        }
      }
    }
    //----------------------------------------------------------------------------------------------------
  }
}
