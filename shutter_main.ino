#include <Encoder.h>
#include <EEPROM.h>
#include <DmxReceiver.h>

#include <Artnet.h>
#include <Ethernet.h>
#include <EthernetUdp.h>
#include <SPI.h>
#include <TeensyMAC.h>

#define encoderPin1 26      //encoderPin1
#define encoderPin2 27      //encoderPin2

#define encoderSwitchPin 28 //push button switch

#define led_pin 33         //status LED Pin
#define EthernetPin 15

Artnet artnet;
byte a1 = 0;
byte b1 = 0;
byte c1 = 0;
byte d1 = 0;

byte ip[] = {10, 200, 0, 10};

void DHCPget(IPAddress address)
{
  ip[0] = address[0];
  ip[1] = address[1];
  ip[2] = address[2];
  ip[3] = address[3];
}

DmxReceiver DMX = DmxReceiver();

int maxavaliableencoderpositions = 4;
int minavaliableencoderpositions = 1;

Encoder myEnc(encoderPin1, encoderPin2);

long oldPosition  = -999;
int encodervaluethen = -999;

bool goback = 0;

int a = 0;

int dmxaddress_zw = -99;
bool dmxalter = 0;

bool turnoffalter = 0;
int turnoff_zw = -99;

bool fulltimealter = 0;
bool holdvoltagealter = 0;

unsigned int fulltime_zw = -99;
unsigned int holdvoltage_zw = -99;

bool buttonState = 1;
bool lastButtonState;

bool buttonstate2 = 0;  //buttonstate for switch command

char x = 'x';   //char for display commands

int encodervaluenow = 0;
int oldencoder = 0;

int i = 0;
float level = 0;

int fulltimeset = 20;
int holdvoltageset = 100;

int state = 0;
int menupos = 1;
int lcdbrightness = 0;
int lastmenupos = -1;

elapsedMillis menusession;
elapsedMillis timerencoder;
elapsedMillis timer;
elapsedMillis timerbutton;
elapsedMillis hbridgetimer;
elapsedMillis updatetimer;

elapsedMillis controller;
int shit = 0;


int dmxaddress;

int dmxzw = 0;
int dmxzw2 = 0;
int dmxzw3 = 0;

int fulltime1 = 0;
int fulltime2 = 0;
int fulltime3 = 0;
int fulltime4 = 0;

int holdvoltage1 = 0;
int holdvoltage2 = 0;
int holdvoltage3 = 0;
int holdvoltage4 = 0;

int turnoff1 = 0;
int turnoff2 = 0;
int turnoff3 = 0;
int turnoff4 = 0;

bool dmxtype = 1;

int powertime = 400;

bool edit = 0;
bool menuactive = 1;
bool firsttime = 1;

unsigned int fulltime = 20;
unsigned int holdvoltage = 100;
unsigned int turnoff = 0; //in Prozent
float turnoffperc = 0;

float floathelp[11][2];

unsigned int dmxvalue = 0;

int DMXbuffer[513];
int DMXbuffer1[513];
int DMXbuffer2[513];

int DMXbufferold[513];
int DMXbufferold1[513];
int DMXbufferold2[513];

int DMXvalues[10];
int DMXvalues1[10];
int DMXvalues2[10];

int DMXvaluesold[10];
int DMXvaluesold1[10];
int DMXvaluesold2[10];

int magtimer [11][3];

int help[10];

int ad1 = 0;
int ad2 = 0;

int valuehb = 0;

int hbridgedes[10][2] = {
  {9, 10}, //H Bridge 1
  {7, 8}, //H Bridge 2
  {23, 6}, //H Bridge 3
  {3, 2}, //H Bridge 4
  {5, 4}, //H Bridge 5
  {29, 30}, //H Bridge 6
  {38, 14}, //H Bridge 7
  {36, 37}, //H Bridge 8
  {20, 35}, //H Bridge 9
  {22, 21}, //H Bridge 10
};

int lastupdate = 0;

//----------------------------------------------------------------------------------------------------------------------------

void setup() {

  for (i = 0; i < 10; i++)
  {
    pinMode(hbridgedes[i][0], OUTPUT);
    pinMode(hbridgedes[i][1], OUTPUT);
  }

  for (i = 0; i < 10; i++)
  {
    help[i] = 0;
    changeHbridgestatus(i, 0);
  }

  delay(500);
  teensySerial();
  teensyMAC();

  uint8_t mac[6];

  teensyMAC(mac);

  Ethernet.init(15);
  delay(100);

  if (Ethernet.begin(mac) == 0)
  {
    //Serial4.print("moin");
    //Serial4.println("Failed to configure Ethernet using DHCP");
    delay(100);
  }

  else
  {
    //Serial4.print("DHCP IP address: ");
    //Serial4.println(Ethernet.localIP());
    DHCPget(Ethernet.localIP());
    delay(100);
  }

  //Serial4.print("1. ARTNET IP address: ");
  //Serial4.println(Ethernet.localIP());

  artnet.begin(mac, ip);

  //Serial4.print("2. ARTNET IP address: ");
  //Serial4.println(Ethernet.localIP());

  DMX.begin();

  Serial.begin(115200);

  Serial4.begin(9600);

  pinMode(encoderSwitchPin, INPUT_PULLUP);

  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);

  pinMode(led_pin, OUTPUT);
  digitalWrite(led_pin, HIGH);

  attachInterrupt(encoderSwitchPin, buttonpush, CHANGE);

  dmxzw = EEPROM.read(0); //read DMX Address
  dmxzw2 = EEPROM.read(1);
  dmxzw3 = EEPROM.read(2);

  fulltime1 = EEPROM.read(4); //read Fulltime
  fulltime2 = EEPROM.read(5);
  fulltime3 = EEPROM.read(6);
  fulltime4 = EEPROM.read(7);

  holdvoltage1 = EEPROM.read(9); //read holdvoltage
  holdvoltage2 = EEPROM.read(10);
  holdvoltage3 = EEPROM.read(11);
  holdvoltage4 = EEPROM.read(12);

  turnoff1 = EEPROM.read(14); //read turnoff
  turnoff2 = EEPROM.read(15);
  turnoff3 = EEPROM.read(16);
  turnoff4 = EEPROM.read(17);

  analogWriteFrequency(5, 1100); //configure PWM Frequencys(all PWM Pins) standard: 200 Hz
  analogWriteFrequency(3, 1100);
  analogWriteFrequency(29, 1100);
  analogWriteFrequency(2, 1100);

  blinkingcursor(0);
  clearlcd();

  fadeinlcd();

  while (menusession < 4000)
  {
    setcursor(2);
    Serial4.print("Consortium ET");
    setcursor(3);
    movecursorright(5);
    Serial4.print("Shutter V2.4.");
  }

  dmxaddress_read_from_eeprom(); //get DMX address
  fulltime_read_from_eeprom();   //get fulltime
  holdvoltage_read_from_eeprom();//get dutycycle
  turnoff_read_from_eeprom();

  valuehb = map(holdvoltage, 1, 100, 1, 256);

  turnoffperc = ((float)turnoff) / 100;

  menusession = 0;

  clearlcd();
}

//----------------------------------------------------------------------------------------------------------------------------

void loop() {

  updateMagnets();
  lastupdate = updatetimer;

  //------------ STATE MACHINE ------------ STATE MACHINE ------------ STATE MACHINE ------------ STATE MACHINE ------------

  switch (state)
  {
    case 0: //Menu

      if (lcdbrightness <= 2)
      {
        fadeinlcd();
      }

      while (menusession < 5000)
      {
        if (menuactive == 1)
        {
          menu();
        }

      }

      if (lcdbrightness == 8)
      {
        clearlcd();
        setcursor(2);
        Serial4.print("DMX Address:");
        showdmxaddress();
        fadeoutlcd();
      }

      enddmxalter();
      endfulltimealter();
      endturnoffalter();

      menupos = 10;
      menuactive = 0;
      state = 1;

      break;

    //--------------------------------------------------------------------------------------------------------------------------------------------------

    case 1: //wait for DMX Signale

      DMX.bufferService(); //load buffer
      //Serial.println("listening");

      //Serial.println("Connected");


      //read DMX Values NORMAL

      if (DMX.newFrame()) //check if new Frame has arrieved
      {
        a = 0;

        for (int i = dmxaddress; i < (dmxaddress + 10); i++)
        {
          DMXbuffer1[i] = DMX.getDimmer(i);  //fill buffer array with 10 DMX values
        }

        for (int i = dmxaddress; i < (dmxaddress + 10); i++)
        {
          DMXvalues1[a] = DMXbuffer1[i];
          a++;
        }
      }

      //read DMX Values ARtnet

      if (artnet.read() == ART_DMX)
      {
        a = 0;

        for (int i = dmxaddress; i < (dmxaddress + 10); i++)
        {
          DMXbuffer2[i] = artnet.getDmxFrame()[i];  //fill buffer array with 512 DMX values
        }

        for (int i = dmxaddress; i < (dmxaddress + 10); i++)
        {
          DMXvalues2[a] = DMXbuffer2[i];
          a++;
        }
      }

      //Check if any Value is bigger than other DMX form

      for (int i = 0; i < 10; i++)
      {
        if (DMXvalues1[i] <= (DMXvalues2[i] + 1))
        {
          DMXvalues[i] = DMXvalues2[i];//use ARTNET
        }

        else       //decide which form to choose
        {
          DMXvalues[i] = DMXvalues1[i];//use normal DMX
        }
      }

      for (int i = 0; i < 10; i++)
      {
        if (DMXvalues[i] != DMXvaluesold[i])  //check if a DMX value has changed
        {
          if ((DMXvalues[i] >= 128) && (DMXvaluesold[i] < 128) && (magtimer[i][1] != 1)) //check if Hbridge recieved ON command and isn´t allready turned on
          {
            magtimer[i][1] = 1;
            magtimer[i][0] = hbridgetimer;
            help[i] = 0;
          }

          else
          {
            if (magtimer[i][1] != 0 && DMXvalues[i] < 128 && DMXvaluesold[i] >= 128)
            {
              magtimer[i][1] = 0;
              magtimer[i][2] = hbridgetimer;
              help[i] = 1;
            }
          }
        }
      }

      for (int i = 0; i < 10; i++)
      {
        DMXvaluesold[i] = DMXvalues[i];
      }


      if ((buttonstate2 == 1) && (timer > 500))
      {
        timer = 0;
        menuactive = 1;
        menusession = 0;
        menupos = 1;
        state = 0;
        break;
      }

      break;

    case 2:  // WITHOUT BI-MAGNET

      //control magnet---------------------------------------------------

      //Serial.println("ACTIVATET");

      if ((buttonstate2 == 1) && (timer > 500))
      {
        timer = 0;
        menuactive = 1;
        menusession = 0;
        menupos = 1;
        state = 0;
        break;
      }

      state = 1;

      break;

    case 3: // WITH BI-MAGNET

      //control magnet-------------------------------------------------

      //Serial.println("ACTIVATET WITH BI MAGNET");

      if ((buttonstate2 == 1) && (timer > 500))
      {
        timer = 0;
        menuactive = 1;
        menusession = 0;
        menupos = 1;
        state = 0;
        break;
      }

      state = 1;

      break;
  }
}

